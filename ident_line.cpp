#include "glexer.h"

bool ident_line(const string s)
{
  const int W1 = 11;
  const int W2 = 12;
  const int W3 = 13;
  const int W4 = 14;
  const int WF = 0;

  const int E1 = 21;
  const int E2 = 22;
  const int E3 = 23;
  const int EF = 1;

  // const int N1 = E1; //SAME 1st CHAR AS ELSE
  const int N2 = 32;
  const int NF = 2;

  const int P1 = 41;
  const int P2 = 42;
  const int P3 = 43;
  const int P4 = 44;
  const int PF = 3;

  const int SSRT = 4;
  const int SBAD = 7;

  unsigned int i = 0;
  unsigned int st = SSRT;
  bool rst;

  queue<char> chars;
  queue<int> states;

  while (i < s.length() && st != SBAD)
  {
    switch (st)
    {
    case SSRT:
      if (s[i] == KEYWORDS[0][0])
        st = W1;
      else if (s[i] == KEYWORDS[1][0])
        st = E1;
      else if (s[i] == KEYWORDS[3][0])
        st = P1;
      else
        st = SBAD;
      break;

    case W1:
      if (s[i] == KEYWORDS[0][1])
        st = W2;
      else
        st = SBAD;
      break;

    case W2:
      if (s[i] == KEYWORDS[0][2])
        st = W3;
      else
        st = SBAD;
      break;

    case W3:
      if (s[i] == KEYWORDS[0][3])
        st = W4;
      else
        st = SBAD;
      break;

    case W4:
      if (s[i] == KEYWORDS[0][4])
        st = WF;
      else
        st = SBAD;
      break;

    case E1:
      if (s[i] == KEYWORDS[1][1])
        st = E2;
      else if (s[i] == KEYWORDS[2][1])
        st = N2;
      else
        st = SBAD;
      break;

    case E2:
      if (s[i] == KEYWORDS[1][2])
        st = E3;
      else
        st = SBAD;
      break;

    case E3:
      if (s[i] == KEYWORDS[1][3])
        st = EF;
      else
        st = SBAD;
      break;

    case N2:
      if (s[i] == KEYWORDS[2][2])
        st = NF;
      else
        st = SBAD;
      break;

    case P1:
      if (s[i] == KEYWORDS[3][1])
        st = P2;
      else
        st = SBAD;
      break;

    case P2:
      if (s[i] == KEYWORDS[3][2])
        st = P3;
      else
        st = SBAD;
      break;

    case P3:
      if (s[i] == KEYWORDS[3][3])
        st = P4;
      else
        st = SBAD;
      break;

    case P4:
      if (s[i] == KEYWORDS[3][4])
        st = PF;
      else
        st = SBAD;
      break;

    default:
      st = SBAD;
    }
    chars.push(s[i]);
    states.push(st);
    i++;
  }

  /*
cout << "Ch:\t";
while (!chars.empty())
{
cout << chars.front() << "\t";
chars.pop();
}
cout << endl << "St:\t";
while (!states.empty())
{
cout << states.front() << "\t";
states.pop();
}
cout << endl;
*/

  if (i == s.length() && st < KEYWORDS_SZ && st != SBAD)
    rst = true;
  else
    rst = false;

  return rst;
}
