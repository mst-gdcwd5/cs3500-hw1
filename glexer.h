#pragma once

#include <iostream>
#include <queue>
#include <string>

using namespace std;

const string LINE_TYPES[] = {"Integer.",     "Decimal.", "Scientific.",
                             "Hexadecimal.", "Binary.",  "Keyword.",
                             "Identifier.",  "Invalid!"};
const int LINE_TYPES_SZ = 8;

const string KEYWORDS[] = {"while", "else", "end", "print"};
const int KEYWORDS_SZ = 4;

string type_line(const string s);
bool ident_line(const string s);
