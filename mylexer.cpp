// cs3500-hw1-cpp.cpp : Defines the entry point for the console application.
//

#include "glexer.h"

int main()
{
  string fline;
  int j, i;

  while (cin >> fline && fline != "STOP")
  {
    if (type_line(fline) == LINE_TYPES[0])
    {
      j = stoi(fline);
      cout << j << endl;
    }
    else
    {
      cout << "missing line count" << endl;
      j = 0;
    }

    i = 0;
    while (i < j && cin >> fline)
    {
      cout << i + 1 << ": " << type_line(fline) << endl;
      i++;
    }
  }

  return 0;
}
